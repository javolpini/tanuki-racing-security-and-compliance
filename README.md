# Tanuki Racing Application - Security and Compliance

**Security and Compliance** - This lab focuses on GitLab's Security and Compliance featureset, and discusses how GitLab facilitates shifting Security Left, building Compliance Frameworks within GitLab, managing Scan Execution and Result policies, generating SBOMs within GitLab, and managing Audit Events within GitLab.

